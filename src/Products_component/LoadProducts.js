import React from "react";
import "./Products.css"
import Modal from "../Modal_component/Modal";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux"
import Cart from "../Router/Cart";
import Favorite from "../Router/Favorite";
import PropsProducts from "./PropsProducts";
import {modalAddToggle, modalRemoveToggle} from "../store/modal/actions"
import {removeProductFromFavorite, removeFromCart, addProductToCart, addProductToFavorite} from "../store/products/actions"



const mapStateToProps = (store) => {
    return{
        products: store.product.products,
        productsIsInCart: store.product.products.filter((product) => product.isInCart),
        productIsInFavorite: store.product.products.filter((product) => product.isInFavorites),
        isLoading: store.product.isLoading,
        error: store.product.error,
        isAddModalOpen: store.modal.isAddModalOpen,
        isRemove: store.modal.isRemoveModalOpen,
        currentProductId: store.modal.currentProductId
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        addProductToCart: (id) => dispatch(addProductToCart(id)),
        removeFromCart: (id) => dispatch(removeFromCart(id)),
        addProductToFavorite: (id) => dispatch(addProductToFavorite(id)),
        removeProductFromFavorite: (id) => dispatch(removeProductFromFavorite(id)),
        modalAddToggle: (id) => dispatch(modalAddToggle(id)),
        modalRemoveToggle: (id) => dispatch(modalRemoveToggle(id))
    }
}



export const LoadProducts = connect(mapStateToProps, mapDispatchToProps)((props) => {
    
const {products, currentProductId, removeFromCart, modalRemoveToggle, isRemove, isAddModalOpen, productIsInFavorite, productsIsInCart,  addProductToFavorite, removeProductFromFavorite, modalAddToggle, addProductToCart} = props;
        return(
            <div className="products"> 
                <Switch>
                    <Route exact path="/">
                        <PropsProducts product = {products}
                        handleModalOpen = {() => modalAddToggle()}
                        addProductToFavorite = {(currentProductId) => {addProductToFavorite(currentProductId)}}
                        removeProductFromFavorite = {(currentProductId) => {removeProductFromFavorite(currentProductId)}}
                    />
                    <Modal             
                        id = {currentProductId}
                        isOpen = {isAddModalOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {() =>{modalAddToggle()}}
                        onSubmit = {() =>{modalAddToggle(); addProductToCart(currentProductId)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                    </Route>
                <Route path="/cart">
                    <Cart 
                        products = {productsIsInCart}
                        handleModalOpen = {(currentProductId) => {modalRemoveToggle(currentProductId)}}
                        addProductToFavorite = {(currentProductId) => {addProductToFavorite(currentProductId)}}
                        removeProductFromFavorite = {(currentProductId) => {removeProductFromFavorite(currentProductId)}}
                        buttonTitle = 'remove'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isRemove}
                        header = {"Хотите удалить с корзины?"}
                        onCancel = {() =>{modalRemoveToggle()}}
                        onSubmit = {() =>{
                            removeFromCart(currentProductId)
                            modalRemoveToggle()
                        }}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                </Route>
                <Route path="/favorite">
                    <Favorite 
                        products = {
                            productIsInFavorite
                        //     products.filter((product) => {
                        //     if(productIsInFavorite){
                        //         // return product.isInFavorites
                        //     }
                        //     return product.isInFavorites
                        // })
                    }
                        handleModalOpen = {() => {modalAddToggle()}}
                        addProductToFavorite = {() => {addProductToFavorite(currentProductId)}}
                        removeProductFromFavorite = {(currentProductId) => {removeProductFromFavorite(currentProductId)}}
                        buttonTitle = 'add'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isAddModalOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {() =>{modalAddToggle()}}
                        onSubmit = {() =>{addProductToCart(currentProductId)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"}    
                                            
                    />
                </Route>
                </Switch>
            </div>
            
        )
        })
        

export default LoadProducts;