import React from "react";
import Button from "../Button_component/Button";
import {AiOutlineStar} from "react-icons/ai";

import { modalAddToggle } from '../store/modal/actions'
import { connect } from 'react-redux'
import {addProductToFavorite, removeProductFromFavorite} from "../store/products/actions";

const mapDispatchToProps = (dispatch) => {
    return {
        modalAddToggle: (maybeId) => dispatch(modalAddToggle(maybeId)),
        addProductToFavorites: (id) => dispatch(addProductToFavorite(id)),
        removeProductFromFavorite: (id) => dispatch(removeProductFromFavorite(id))
    }}

    export const Product = connect(null, mapDispatchToProps)((props) => {
        const {
            title,
            price,
            url,
            article,
            color,
            id,
            isInFavorites,
        } = props
    


    return(
        <div key = {props.index} className="product_position">  
            <div className="products_set">
            <p className="product_name">{title}</p>
            <p className="product_price">{price}</p>
            <img className="product_img" src={url} alt="product_image"/>
            <p className="product_article">Article: {article}</p>
            <p className="product_color">Color: {color}</p>
            <Button className="propsBtn"
                onClick = {() => {props.modalAddToggle(id)}}
                text = {"Add to cart"}>
            </Button>
            <AiOutlineStar
                onClick = {() => isInFavorites ? props.removeProductFromFavorite(id) : props.addProductToFavorite(id)}
                className = {isInFavorites ? "favorite" : " "}/>
            </div>
        </div>
    )
})

