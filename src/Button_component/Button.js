import React from "react";

function Button(props){
        return(
        <button className={props.className} 
        text={props.text} 
        title={props.title} 
        onClick={props.onClick}>{props.text}</button>
        )
    }


export default Button;