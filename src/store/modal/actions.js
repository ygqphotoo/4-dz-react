import { MODAL_ADD_TOGGLE, MODAL_REMOVE_TOGGLE } from './types'


export const modalAddToggle = (maybeId) => {
    return{
        type: MODAL_ADD_TOGGLE,
        payload: maybeId,
   }
}

export const modalRemoveToggle = (maybeId) => {   
    return{
        type: MODAL_REMOVE_TOGGLE,
        payload: maybeId,
    }
}

