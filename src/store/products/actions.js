import {GET_PRODUCTS_INIT, 
        GET_PRODUCTS_ERROR, 
        GET_PRODUCTS_SUCCESS, 
        TOGGLE_PRODUCT_TO_CART, 
        TOGGLE_PRODUCT_TO_FAVORITES} from "./types"
import { store } from "../index"

const getProductsError = (error) => {
    return {
        type: GET_PRODUCTS_ERROR, 
        payload: error
    }
}

const getProductsInit = () =>{
    return {
        type: GET_PRODUCTS_INIT 
    }
}

const getProductsSuccess = (products) =>{
    return{
        type: GET_PRODUCTS_SUCCESS,
        payload: products
    }
}

const  checkProducts = (products, cart, favorite) => {
    const updatedProducts = products.map(product =>{
        const productInCart = cart.some(({id}) => id === product.id)
        const productInFavorite = favorite.some(({id}) => id === product.id)
        return {...product, 
            isInCart: productInCart, 
            isInFavorites: productInFavorite}
    })
    return updatedProducts
}

function updatedStorage (updatedProducts) {
    updatedProducts.forEach(element => {
        if(element.isInCart === "true"){
            localStorage.setItem("cart", JSON.stringify(updatedProducts));
        }
    });
}

export const addProductToCart = (id) => {
    const state = store.getState();
    const products = state.product.products;
    const cart = JSON.parse(localStorage.getItem("cart"))
    const product = products.find((item) => item.id === id)
    cart.push(product)
    localStorage.setItem("cart", JSON.stringify(cart))
    
    return {
        type: TOGGLE_PRODUCT_TO_CART,
        payload: product
    }
}


export const addProductToFavorite = (id) =>{    
    const state = store.getState();
    const products = state.product.products;
    const favorite = JSON.parse(localStorage.getItem("favorite"))
    const product = products.find((product) => product.id === id)          
    favorite.push(product)
    
    localStorage.setItem("favorite", JSON.stringify(favorite))

        return {
            type: TOGGLE_PRODUCT_TO_FAVORITES,
            payload: product     
        }
    }
    
export const  removeProductFromFavorite = (id) =>{
    const state = store.getState();
    const products = state.product.products;
    
    const favorite = JSON.parse(localStorage.getItem("favorite"))
    const newFavorites = favorite.filter((product) => {
        return id !== product.id
    })
    
    localStorage.setItem("favorite", JSON.stringify(newFavorites))
    const product = products.find((product) => product.id === id)
            return {
                type: TOGGLE_PRODUCT_TO_FAVORITES,
                payload: product
            }
}
export const removeFromCart = (id) =>{
    const state = store.getState();
    const products = state.product.products;
    
    const productToDelete = JSON.parse(localStorage.getItem('cart'))
    const newCart = productToDelete.filter((product) => {
        return id !== product.id
    })

    localStorage.setItem('cart', JSON.stringify(newCart))
    const oneProduct = products.find((product) => product.id === id)
        return {
            type: TOGGLE_PRODUCT_TO_CART,
            payload: oneProduct
        }   
    }


export const getProducts = () => async (dispatch) => {
    dispatch(getProductsInit())
    if (!localStorage.getItem("cart") || !localStorage.getItem("favorite"))  {
        localStorage.setItem("cart", JSON.stringify([]))
        localStorage.setItem("favorite", JSON.stringify([]))
        }
        try{
            const response = await fetch("./Product.json")
            const data = await response.json()
            const localCart = JSON.parse(localStorage.getItem("cart"))
            const localFavorite = JSON.parse(localStorage.getItem("favorite"))
            const updatedProducts = checkProducts(data.products , localCart , localFavorite)
            updatedStorage(updatedProducts)
            dispatch(getProductsSuccess(updatedProducts))
        } catch(error){
            dispatch(getProductsError(error))
        }
}

