import React from "react";
import LoadProducts from "./Products_component/LoadProducts";
import "./Products_component/Products.css"
import {connect} from "react-redux"
import { useEffect } from "react"
import { getProducts as getProductsAction} from "./store/products/actions";
import { BrowserRouter as Router, Link } from "react-router-dom"

function App (props){
  useEffect(() => {
    props.getProducts()
  }, [props]) 

    return(
      <Router>
        <div className="App">
          <div className="link_position">
          <Link className="header_link" to="/">Products</Link>
          <Link className="header_link" to="/cart">Cart</Link>
          <Link className="header_link" to="/favorite">Favorite</Link>
          </div>
          <LoadProducts  />
        </div>
      </Router>
    )
  }


  const mapStateToProps = (store) => {
    return {
      products: store.product.products,
      isLoading: store.product.isLoading,
      error: store.product.error
    }
  }
  const mapDispatchToProps = (dispatch) => {
    return{
      getProducts: () => dispatch(getProductsAction())
    }
  }


export default connect(mapStateToProps, mapDispatchToProps)(App);
